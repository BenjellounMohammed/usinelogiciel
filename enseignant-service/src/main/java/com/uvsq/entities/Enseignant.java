package com.uvsq.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@ApiModel(description = "All details about the enseignant. ")
@Table(name = "enseignants")
public class Enseignant implements Serializable {


	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@ApiModelProperty(notes = "The database generated employee ID")
	private Long id;

	@ApiModelProperty(notes = "The last name of enseignant")
	private String nom;

	@ApiModelProperty(notes = "The first name of enseignant")
	private String prenom;

	@ApiModelProperty(notes = "The email of enseignant")
	private String email;

	@ApiModelProperty(notes = "The number of enseignant")
	private int numero;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Enseignant(Long id, String nom, String prenom, String email, int numero) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.numero = numero;
	}
	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}






}
