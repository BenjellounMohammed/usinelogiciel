package com.uvsq.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(description = "All details about the modules. ")
@Table(name = "modules")
public class Module implements Serializable {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@ApiModelProperty(notes = "The database generated module ID")
	private Long id;
	
	@ApiModelProperty(notes = "The last name of module")

	private String nom;
	
	@ApiModelProperty(notes = "The departement name of modules ")

	private String departement;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	public Module(Long id, String nom, String departement) {
		super();
		this.id = id;
		this.nom = nom;
		this.departement = departement;
	}
	public Module() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	
}
