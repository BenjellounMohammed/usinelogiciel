package com.uvsq.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uvsq.dao.IModuleRepository;
import com.uvsq.entities.Module;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/module")
@Api(value="Module Management System", description="Operations for Module")

public class ModuleController {

	@Autowired
	private IModuleRepository moduleRepository;

	@GetMapping("/test")
	public String test() {
		return "test";
	}

	@PostMapping("/ajouter")
	@ApiOperation(value = "Add Module")
	public Module ajouterModule(@RequestParam(value="nom_module") String nom , @RequestParam(value="departement") String departement) {
		Module m = new Module();
		m.setDepartement(departement);
		m.setNom(nom);
		moduleRepository.save(m);
		return m;
	}
	
	@GetMapping("/afficherAll")
	@ApiOperation(value = "View a list of available Module", response = List.class)
	public List<Module> getModules(){
		return moduleRepository.findAll();
	}


	@GetMapping("/consulterModuleParNom")
	@ApiOperation(value = "View a list of Module by name", response = List.class)
	public List<Module> getModule(String nom){
		return moduleRepository.findByNom(nom);
	}

	@DeleteMapping("/delete")
	@ApiOperation(value = "Delete an module from database")
	public Boolean delete(Long id){
		moduleRepository.deleteById(id);
		return true;
	}

	@PutMapping("/update")
	@ApiOperation(value = "Update information of a specific module")
	public Module update (Module e){
		moduleRepository.saveAndFlush(e);
		return e;
	}

}
