package com.uvsq.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uvsq.entities.Enseignant;

public interface IEnseignantRepository extends JpaRepository<Enseignant, Long> {

	@Query("select e from Enseignant e where e.nom like :x")
	public List<Enseignant> enseignantParMc(@Param("x")String mc);
	
	public List<Enseignant> findByNom (String nom);
	

}
