package com.uvsq.controllers;

import com.uvsq.dao.IEnseignantRepository;
import com.uvsq.entities.Enseignant;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/enseignant/")
@Api(value="Enseignant Management System", description="Operations for enseignant")

public class EnseignantController {

	@Autowired
	private IEnseignantRepository enseignantRepository;

	@GetMapping("/test")
	public String test() {
		return "test";
	}

	@PostMapping("/ajouter")
	@ApiOperation(value = "Add Enseignant")
	public Enseignant ajouterEnseignant(@RequestParam(value="nom_enseignant") String nom , @RequestParam(value="prenom_enseignant") String prenom ,
@RequestParam(value="mail_enseignant") String email , @RequestParam(value="numero_enseignant") int numero) {
		Enseignant e = new Enseignant();
		e.setNom(nom);
		e.setPrenom(prenom);
		e.setEmail(email);
		e.setNumero(numero);
		enseignantRepository.save(e);
		return e;
	}
	@GetMapping("/afficherAll")
	@ApiOperation(value = "View a list of available enseignant", response = List.class)
	public List<Enseignant> getEnseignants(){
		return enseignantRepository.findAll();
	}

	@GetMapping("/afficherPage")   // afficher tt les enseignants 5 page par 5 page
	@ApiOperation(value = "View a list of 5  enseignants", response = List.class)

	public Page<Enseignant> getEnseignants(int page){
		return enseignantRepository.findAll(new PageRequest(page, 5));
	}

	/*@RequestMapping("/consulterEnseignantParMc")
	public List<Enseignant> getEnseignants(String mc){
		return enseignantRepository.enseignantParMc("%"+mc+"%");
	}*/

	@GetMapping("/consulterEnseignantParNom")
	@ApiOperation(value = "View a list of enseignants by name", response = List.class)
	public List<Enseignant> getEnseignant(String nom){
		return enseignantRepository.findByNom(nom);
	}

	@GetMapping("/consulterunEnseignant")  // Afficher un seul enseignant par son ID
	@ApiOperation(value = "View an enseignant by id")
	public Optional<Enseignant> getEnseignant(Long id){
		return enseignantRepository.findById(id);
	}

	@DeleteMapping("/delete")
	@ApiOperation(value = "Delete an enseignant from database")

	public Boolean delete(Long id){
		enseignantRepository.deleteById(id);
		return true;
	}

	@PutMapping("/update")
	@ApiOperation(value = "Update information of a specific enseignant")
	public Enseignant update (Enseignant e){
		enseignantRepository.saveAndFlush(e);
		return e;
	}

}
