package com.uvsq.dao;

import com.uvsq.entities.Module;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface IModuleRepository extends JpaRepository<Module, Long> {

	
	public List<Module> findByNom (String nom);
	

}
