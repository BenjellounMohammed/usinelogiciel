## Projet Usine Logiciel basé sur Spring Boot et l'architecture microservices
L'objectif du projet et de faire une application qui contient deux services Modules et Enseignants, et la création d'un troisième service pour faire l'appel 
et la liaison entre deux services.

Pour l'architecture du projet : 

>  Création d'un projet Spring Boot

>  Création des entités JPA

>  Configurer les fichiers application.properties des deux services

>  Création des interfaces JPA Repository basées sur Spring Data

>  Tester la couche DAO

>  Création et Tester la couche métier Interface et Implémentation

>  Création des controller pour implementer les méthodes du service 

>  finalement j'ai utiliser la spécification OpenApi Swagger pour faire la documentation et il offre également 

>  une interface permettant d’explorer et tester les différentes méthodes offertes par le service

### Docker 

les Principaux éléments de Docker : 

* Containers : répertoires contenant toute votre application

* Images : image de conteneurs

* Dockerfiles : scripts automatisant le processus de création des images

J'ai utiliser Docker pour faire lancer l'application dans des conteneurs en utilisant des commandes suivant : 

Dans le dossier docker-compose vous trouver le fichier [docker-compose.yml] 

Son role c'est de regrouper les deux services le services [mysql] pour la partie base de donnée

> et la partie [ phpMyadmin ] pour avoir une interface de gestion de basse de donnée

> la partie [parent_modules] qui permet de builder le projet et générer les artifacts des deux services modules et enseignants

> après il y a le service enseignant son role c'est de démarer le JAR applicatif du service enseignant

> après il y a le service module son role c'est de démarer le JAR applicatif du service Module

> Dans le dossier docker-compose vous trouver le fichier [ .env] 

> son role c'est de setter les variables d'environnement dans le système la commande suivante 

```sh
$ source .env
```

## Executer l'application avec [docker]

pour démarer l'application :
il suffit juste de se mettre dans le dossier docker-Compose par la commande :  
```sh
$ cd docker-compose
```

et pour lancer l'execution des differents services il faut juste executrer cette commande :
```sh
$ docker-compose up 
```

après pour vérifier le bon exécution des differents service :
d'abord pour [PHPMyadmin] par l'url : [http://locahost:8989]
après pour pouvoir s'authentifier il faut utiliser les données suivantes :
```sh
serveur : mysql
login : dev
pwd : dev
```

### Swagger
vous trouverez la configuration pour l'interface de Swagger dans la classe Swagger2Config.java, ainsi que dans le controller de chaque service.

pour tester le service Enseignant avec ces differentes méthodes il faut utiliser l'url suivant :
[http://localhost:8082/enseignant-ws/swagger-ui.html]

pour tester le services Module ces differentes méthodes il faut utiliser l'url suivant :
[http://localhost:8083/module-ws/swagger-ui.html]


### télechargement et installation de docker sous unix :

```sh
$ sudo apt-get install docker-engine

```
Le pare-feu par défaut d'Ubuntu (UFW: Uncomplicated Firewall) refuse tout le trafic de transfert par défaut, ce qui est nécessaire pour le docker.

Activer le transfert avec UFW:

Mise à jour de la configuration UFW avec l’éditeur de texte nano.
```sh
$ sudo nano /etc/default/ufw

```

### Réferences

https://spring.io/guides/gs/spring-boot/

https://www.supinfo.com/articles/single/6400-installation-utilisation-docker




### BY BENJELLOUN Mohammed